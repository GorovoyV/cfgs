package main

import (
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
)

var (
	app 			= kingpin.New("configs","parse")
	inFNFlag		= app.Flag("in"," input filename").Short('i').String()
	outFNFlag		= app.Flag("out","output filename").Short('o').String()
	hostFlag		= app.Flag("host","host").Short('h').Int()
	portFlag		= app.Flag("port","port").Short('p').Int()

	cfg 			= app.Command("cfg","works with cfg")
	edit			= cfg.Command("edit","edits cfg")
)

func run() error {
	cmd,err := app.Parse(os.Args[1:])
	switch kingpin.MustParse(cmd,err) {
	case edit.FullCommand():
		editConfig(*inFNFlag)
	}
	return nil
}
func main() {
	if err := run(); err != nil {
		panic(err)
	}
}

