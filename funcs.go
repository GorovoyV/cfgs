package main

import (
	"encoding/json"
	"fmt"
	"github.com/golang/protobuf/proto"
	"github.com/hyperledger/fabric-config/configtx"
	cb "github.com/hyperledger/fabric-protos-go/common"
	"io/ioutil"
)

func editConfig(filename string)  {
	/*jsonFile, err := os.Open("config.channel.json")


	if err != nil {
		fmt.Println(err)
	}

	defer jsonFile.Close()
	var result map[string]interface{}
	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal([]byte(byteValue), &result)

	//pb, err := os.Open("config_block.pb")
*/
	//h := common.ConfigEnvelope{}
	//fmt.Println(h.Config)
	//bV,_ := ioutil.ReadAll(pb)
	//var msg proto.Message
	//h = proto.Unmarshal(bV,msg)
	//h.XXX_Unmarshal(bV)
	//fmt.Println(h.Config)

	/*ap := AP{
		host: "eximpeer.nbu.tokend.io",
		port: 2011,
	}
	var apArr []AP
	apArr = append(apArr, ap)
	val := value{anchor_peers: apArr}
	anchorPeers := AnchorPeers{
		mod_policy: "Admin",
		value: val,
	}*/

	//vals := values{AnchorPeers: anchorPeers}
	//fmt.Println(result["channel_group"])
/*	q := result["channel_group"].(map[string]interface{})
	w := q["groups"].(map[string]interface{})
	e := w["Application"].(map[string]interface{})
	r := e["groups"].(map[string]interface{})
	t := r["NBUOrg"].(map[string]interface{})
	_ = t["values"].(map[string]interface{})*/
//	hostPort := make(map[string]interface{})
//	hostPort["host"] = "eximpeer.nbu.tokend.io"
//	hostPort["port"] = 2011
//	AnchorPeers := make(map[string]interface{})
	//AnchorPeers["anchor_peers"] = hostPort
	//fmt.Println(t["values"])
	//fmt.Println(t["values"]["MSP"])
//	y["AnchorPeer"]["mod_policy"] = "Admins"
	//y["AnchorPeer"]["value"] = AnchorPeers
	//fmt.Println(y)
	//y["AnchorPeers"] = anchorPeers
	/*var ap1 = map[string][]AP{
		"anchor_peers": apArr,
	}
	//fmt.Println(ap1)
	var val = map[string]map[string][]AP{
		"value": ap1,
	}

	var mod_policy = map[string]string{
		"mod_policy": "Admins",
	}
	ex := AnchorPeer{mod_policy: mod_policy,ap: val}
	/*var AnchorPeers = map[string]AnchorPeer{
		"AnchorPeers": ex,
	}
	//fmt.Println(ex)
	//ap["anchor_peers"] = ap
	y["AnchorPeers"] = ex
	//u := y["MSP"].(map[string]interface{})
	//fmt.Println(t["values"])
	//fmt.Println(y)
	//fmt.Println(result)
	z,err := json.Marshal(result)
	if err!=nil {
		fmt.Println(err)
		return
	}

	//fmt.Println(string(z))
	ioutil.WriteFile("big_marhsall.json", z, os.ModePerm)
*/
	blockBin, err := ioutil.ReadFile("config_block.pb")
	if err != nil {
		panic(err)
	}
	block := &cb.Block {}
	err = proto.Unmarshal(blockBin, block)
	if err != nil {
		panic(err)
	}
	blockDataEnvelope := &cb.Envelope {}
	err = proto.Unmarshal(block.Data.Data[0], blockDataEnvelope)
	if err != nil {
		panic(err)
	}
	blockDataPayload := &cb.Payload {}
	err = proto.Unmarshal(blockDataEnvelope.Payload, blockDataPayload)
	if err != nil {
		panic(err)
	}
	config := &cb.ConfigEnvelope {}
	err = proto.Unmarshal(blockDataPayload.Data, config)
	if err != nil {
		panic(err)
	}

	conf := configtx.New(config.Config)
	org := conf.Application()
	//fmt.Println(conf)
	app := org.Organization("NBUOrg")
	//org.SetOrganization(app)

	address := configtx.Address{Host: "eximpeer.nbu.tokend.io",Port: 2011}
	err = app.AddAnchorPeer(address)
	if err != nil{
		panic(err)
	}
	jsonCfg,_ := json.Marshal(config.Config)
	fmt.Println(string(jsonCfg))
	//fmt.Println(conf)
}
