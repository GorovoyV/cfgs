module cfgs

go 1.15

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/alecthomas/units v0.0.0-20201120081800-1786d5ef83d4 // indirect
	github.com/golang/protobuf v1.3.3
	github.com/hyperledger/fabric-config v0.0.9
	github.com/hyperledger/fabric-protos-go v0.0.0-20210127161553-4f432a78f286
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
